;;;
;;; The first part of this file is not directly related to the life mode but adds 
;;; some necessary things for it to work. Some settings are general settings of 
;;; emacs19.
;;;

;;; what version of emacs ?

(defun emacs19-used ()
  "Returns t if emacs19 is used"
  (and (boundp 'emacs-version)
       (string= (substring emacs-version 0 2) "19")))



;;; hilit-auto-highlight: T if we should highlight all buffers as we find
;;;                       'em, nil to disable. default is t 
(setq hilit-auto-highlight nil)

;;; hilit-auto-highlight-maxout: auto-highlight is disabled in buffers larger
;;;                              than this (default is 60000) 
;;; (setq hilit-auto-highlight-maxout 10000)

;;; hilit-auto-rehilight: If this is non-nil, then hilit-redraw and hilit-recenter 
;;; will also rehighlight part or all of the current buffer.  T will rehighlight the
;;; whole buffer, a NUMBER will rehighlight that many lines before and after
;;; the cursor, and the symbol 'visible' will rehighlight only the visible
;;; portion of the current buffer.  This variable is buffer-local.

(setq hilit-auto-rehighlight 'visible)

;;; hilit-inhibit-rebinding: don't redefine locally C-l and C-y (thus
;;;                          preventing rehighlighting of yanked regions)
;;; default is nil.	
;;; (setq hilit-inhibit-rebinding t)

;;; hilit-quietly: no message when rehighlighting	
;;; default is nil
;;; (setq hilit-quietly t) 


;;; comment region for every emacs dialect
;;; This piece of code comes from the emacs19 release

(if (not (emacs19-used))
    (defun comment-region (beg end &optional arg)
      "Comment the region; third arg numeric means use ARG comment characters.
If ARG is negative, delete that many comment characters instead.
Comments are terminated on each line, even for syntax in which newline does
not end the comment.  Blank lines do not get comments."
      ;; if someone wants it to only put a comment-start at the beginning and
      ;; comment-end at the end then typing it, C-x C-x, closing it, C-x C-x
      ;; is easy enough.  No option is made here for other than commenting
      ;; every line.
      (interactive "r\np")
      (or comment-start (error "No comment syntax is defined"))
      (if (> beg end) (let (mid) (setq mid beg beg end end mid)))
      (save-excursion
	(save-restriction
	  (let ((cs comment-start) (ce comment-end))
	    (cond ((not arg) (setq arg 1))
		  ((> arg 1)
		   (while (> (setq arg (1- arg)) 0)
		     (setq cs (concat cs comment-start)
			   ce (concat ce comment-end)))))
	    (narrow-to-region beg end)
	    (goto-char beg)
	    (while (not (eobp))
	      (if (< arg 0)
		  (let ((count arg))
		    (while (and (> 1 (setq count (1+ count)))
				(looking-at (regexp-quote cs)))
		      (delete-char (length cs)))
		    (if (string= "" ce) ()
		      (setq count arg)
		      (while (> 1 (setq count (1+ count)))
			(end-of-line)
			;; this is questionable if comment-end ends in whitespace
			;; that is pretty brain-damaged though
			(skip-chars-backward " \t")
			(backward-char (length ce))
			(if (looking-at (regexp-quote ce))
			    (delete-char (length ce)))))
		    (forward-line 1))
		(if (looking-at "[ \t]*$") ()
		  (insert cs)
		  (if (string= "" ce) ()
		    (end-of-line)
		    (insert ce)))
		(search-forward "\n" nil 'move))))))))


;;;---------------------------------------------------------------------------
;;; LIFE MODE
;;;


;;; load life-mode and life-emphasize

(load "life-mode.el")

(if (emacs19-used)
    (load "life-emphasize.el"))


;;; life mode hook:
;;;  turn on auto fill, 
;;;  newline causes the currenrt line to be indented,a newline inserted,
;;;          and the next line indented
;;; emphasize if emacs19



(setq life-mode-hook 
      '(lambda() 
	 (progn
	   (if (emacs19-used)
	       (life-emphasize-mode))
	   (local-set-key "\015" 'reindent-then-newline-and-indent)
	   (turn-on-auto-fill))))

;;; add life-mode for life programs

(setq auto-mode-alist (append (list (cons "\\.lf$" 'life-mode)
				    (cons "\\.gr$" 'life-mode)
                                    (cons "\\.in$" 'life-mode)
				    (cons "\\.out$" 'life-mode)
				    (cons "\\.test$" 'life-mode)
				    (cons "\\.exp$" 'life-mode)
				    (cons "\\.[^_]*[_]*life$" 'life-mode))
                              auto-mode-alist))


;; Customization of the major mode
;; 
;;    There are three kinds of comments:
;;     - large comments start with %%%. They are always indented at column 0.
;;     - mid-comments start with %%. 
;;       They are indented as life code, unless there
;;       is a comment on the line before. In the latter case, the comments are 
;;       aligned.
;;     - small comments start with %.
;;       If small-comments is non-nil, they are idented to comment-column 
;;       (default value is 48), unless there is a comment on the line before.
;;       Otherwise, they are idented like mid-comments.
;;       Default of small-comments is nil.

;; (setq comment-column 32)
;; (setq small-comments t)

;; 
;;    TAB may be used as a real tab in some places. If the variable
;;    life-tab-always-indent is not nil, TAB in Life mode should always 
;;    reindent the current line, regardless of where in the line point is when
;;    the TAB command is used. Otherwise, TAB inserts a tab when it is not in 
;;    the indenting region (before the first non void character on a line).   
;;    The default value of this variable is t.

;; (setq life-tab-always-indent nil)

;;    life-brace-offset is the ammount of extra indentation for a line if the 
;;    line before starts with an open brace. Its default value is 4.

;; (setq life-brace-offset 8)

;; life-multiline-offset is the amount of extra indentation for more than one
;; line long sentences (sequence of terms ending with , ; or |). Its default
;; value is 2. 

;; (setq life-multiline-offset 4)


;;; Customization of the highlighting mode
;;; la vie en rose
;;; if you don't want highlighting, set the variable to nil

(if (emacs19-used)
    (setq life-emphasize-flag t))


;;; Faces are created by the life-emphasize mode, that correspond to various 
;;; life tokens:
;;;      life-comment
;;;      life-atom
;;;      life-operator
;;;      life-value
;;;      life-syntax
;;;      life-variable

;;; You can use the following functions to customize the faces used.

;;;  set-face-background
;;;    Change the background color of face FACE to COLOR (a string).
;;;  set-face-font
;;;    Change the font of face FACE to FONT (a string).
;;;  set-face-foreground
;;;    Change the foreground color of face FACE to COLOR (a string).
;;;  set-face-underline-p
;;;    Specify whether face FACE is underlined.  (Yes if UNDERLINE-P 
;;;  is non-nil.)

;;; example

;;;(if life-emphasize-flag 
;;;    (progn
;;;      (set-face-font 'life-comment 
;;;		"-adobe-courier-bold-o-normal--14-100-100-100-m-90-iso8859-1")
;;;      (set-face-background 'life-value "darkslategrey")
;;;      (set-face-foreground 'life-syntax "red")
;;;      (set-face-underline-p 'life-variable t)
;;;      ))

