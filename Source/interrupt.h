/* Copyright 1991 Digital Equipment Corporation.
** All Rights Reserved.
*****************************************************************/

extern long interrupted;

extern void interrupt();
extern void init_interrupt();

extern void handle_interrupt();

