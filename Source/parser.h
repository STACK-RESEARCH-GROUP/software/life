/* Copyright 1991 Digital Equipment Corporation.
** All Rights Reserved.
*****************************************************************/

extern long parse_ok;
extern psi_term parse();

extern ptr_psi_term stack_copy_psi_term();
extern ptr_psi_term heap_copy_psi_term();
