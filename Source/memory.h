/* Copyright 1991 Digital Equipment Corporation.
** All Rights Reserved.
*****************************************************************/

extern GENERIC heap_alloc();
extern GENERIC stack_alloc();

extern void garbage();

extern long memory_check();

extern void fail_all();

extern char *GetStrOption();
extern int GetBoolOption();
extern int GetIntOption();
